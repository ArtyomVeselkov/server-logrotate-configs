# How to use
## Notes
   - Test log is `test_rotatelog.log`
## Example
   - Generate test log and rotate it
   ```bash
   php generate_test_log.php && \
   logrotate -v -d extra/logrotate/migvapor-local/logrotate.conf -s extra/logrotate/migvapor-local/state.tmp
   ```
   - Generate logrotate for project
   ```bash
   bash extra/logrotate/abstract/build.sh \   
        /var/www/migvapor.loc/public_html \ # path to magento root
        /var/www/migvapor.loc/public_html/extra/logrotate/abstract/magento \ # path to template
        /var/www/migvapor.loc/public_html/extra/logrotate/migvapor-local # path where to place rendered configs
   ```