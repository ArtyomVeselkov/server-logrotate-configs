<?php
return ;
//increase execution time
ini_set('max_execution_time', 900); //900 seconds = 15 minutes

//require Magento
require_once 'app/Mage.php';
$app = Mage::app('admin');
umask(0);

//enable Error Reporting
error_reporting(E_ALL & ~E_NOTICE);

function generateRandomString($length = 10) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

try {
    $loopMax = 10000;
    for ($i = 0; $loopMax > $i; $i++) {
        $randomText = generateRandomString(256);
        Mage::log('Default log message: ' . $i . ', date: ' . date('Y/m/d H:i:s') . ', text: ' . $randomText, Zend_Log::INFO, 'test_rotatelog.log');
    }
}
catch(Exception $e) {
    //something went wrong...
    print($e->getMessage());
}
?>
