#!/usr/bin/env bash

# example
# bash extra/logrotate/abstract/build.sh \
#      /var/www/migvapor.loc/public_html \
#      /var/www/migvapor.loc/public_html/extra/logrotate/abstract/magento \
#      /var/www/migvapor.loc/public_html/extra/logrotate/migvapor-local

TARGET_PATH=$1;
INCLUDE_PATH=$2;
BUILD_TO=$3;

echo "DEBUG: mkdir -p ${TARGET_PATH}/archive/oldlog/" &&
mkdir -p "${TARGET_PATH}/archive/oldlog/" &&
echo "DEBUG: mkdir -p ${BUILD_TO}/include/" &&
mkdir -p "${BUILD_TO}/include/"

#echo "DEBUG: sed -e s|{{target_path}}|${TARGET_PATH})|g ${INCLUDE_PATH}/logrotate.conf >${BUILD_TO}/logrotate.conf.tmp" &&
#sed -e "s|{{target_path}}|${TARGET_PATH})|g" "${INCLUDE_PATH}/logrotate.conf" >"${BUILD_TO}/logrotate.conf.tmp"
#sleep 1
#echo "DEBUG: sed -e s|{{include_path}}|${INCLUDE_PATH})|g ${BUILD_TO}/logrotate.conf.tmp >${BUILD_TO}/logrotate.conf" &&
#sed -e "s|{{include_path}}|${INCLUDE_PATH})|g" "${BUILD_TO}/logrotate.conf.tmp" >"${BUILD_TO}/logrotate.conf"
#rm "${BUILD_TO}/logrotate.conf.tmp"

processFile () {
    TARGET_PATH_S=$1;
    INCLUDE_PATH_S=$2;
    BUILD_TO_S=$3;
    TARGET_FILE=$4
    TARGET_FILE_NAME="${TARGET_FILE##*/}"
    echo "DEBUG: file path ${TARGET_FILE}, file name ${TARGET_FILE_NAME}"

    echo "DEBUG: sed -e s|{{target_path}}|${TARGET_PATH_S}|g ${TARGET_FILE} >${BUILD_TO_S}/${TARGET_FILE_NAME}.tmp" &&
    sed -e "s|{{target_path}}|${TARGET_PATH_S}|g" "${TARGET_FILE}" >"${BUILD_TO_S}/${TARGET_FILE_NAME}.tmp"
    echo "DEBUG: sed -e s|{{include_path}}|${BUILD_TO_S})|g ${BUILD_TO_S}/${TARGET_FILE_NAME}.tmp >${BUILD_TO_S}/${TARGET_FILE_NAME}" &&
    sed -e "s|{{include_path}}|${BUILD_TO_S}|g" "${BUILD_TO_S}/${TARGET_FILE_NAME}.tmp" >"${BUILD_TO_S}/${TARGET_FILE_NAME}"
    rm "${BUILD_TO_S}/${TARGET_FILE_NAME}.tmp"
}

processFile "${TARGET_PATH}" "${INCLUDE_PATH}" "${BUILD_TO}" "${INCLUDE_PATH}/logrotate.conf"

for filename in ${INCLUDE_PATH}/include/*; do
    processFile "${TARGET_PATH}" "${INCLUDE_PATH}" "${BUILD_TO}/include/" "${filename}"
done